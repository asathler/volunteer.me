<?php

namespace App;

use App\Models\UserSettings;
use App\Notifications\VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    public function settings()
    {
        return $this->hasOne('App\Models\UserSettings');
    }

    public static function findOrCreate($data)
    {
        return DB::transaction(function () use ($data) {
            $loggedUser = User::where('email', $data['email'])->firstOr(function () use ($data) {
                return User::forceCreate([
                    'uuid' => Str::orderedUuid(),
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => $data['password'],
                    'email_verified_at' => $data['email_verified_at'] ?? null
                ]);
            });

            $settings = UserSettings::updateOrCreate([
                'user_uuid' => $loggedUser->uuid
            ], [
                'avatar' => $data['avatar'] ?? '/img/avatar/default.png',
                'locale' => $data['locale'] ?? 'en'
            ]);

            return $loggedUser;
        });
    }
}
