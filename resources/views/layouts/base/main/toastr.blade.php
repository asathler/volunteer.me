@php
    $sessions = \Illuminate\Support\Facades\Session::all();
    $aceeptedNames = ['error', 'info', 'success', 'warning'];
@endphp
<script>
    $(document).ready(function() {
        toastr.options = {
            "closeButton": true,
            "newestOnTop": false,
            "progressBar": true,
            "timeOut": "6000",
            "extendedTimeOut": "2000",
        };

        @foreach($sessions as $name => $values)
            @if(in_array($name, $aceeptedNames))
                @php($values = (array) $values)
                @foreach($values as $value)
                    toastr.{{ $name }}('{{ $value }}');
                @endforeach
            @endif
        @endforeach
    });
</script>

{{--
// ************************************************************
// Toastr options
// ************************************************************
"closeButton": true,
"debug": false,
"newestOnTop": false,
"progressBar": true,
"positionClass": "toast-top-right",
"preventDuplicates": true,
"onclick": null,
"showDuration": "300",
"hideDuration": "1000",
"timeOut": "6000",
"extendedTimeOut": "2000",
"showEasing": "swing",
"hideEasing": "linear",
"showMethod": "fadeIn",
"hideMethod": "fadeOut"

// ************************************************************
// Toastr samples
// ************************************************************
toastr.error('message error', 'title error');
toastr.info('message info', 'title info');
toastr.success('message success', 'title success');
toastr.warning('message warning', 'title warning');
--}}
