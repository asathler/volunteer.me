<a href="{{ route('dashboard') }}"
   class="brand-link
         {{ config('adminlte.logo-color') }}
         {{ config('adminlte.logo-text-size') }}">

    <img src="{{ asset('img/AdminLTELogo.png') }}"
         alt="{{ env('APP_NAME') }} Logo"
         title="{{ env('APP_NAME') }} Logo"
         class="brand-image img-circle elevation-3"
         style="opacity: .8">

    <span class="brand-text font-weight-light">
        <strong>{{ env('APP_NAME_BASE') }}</strong>{{ env('APP_NAME_SUFFIX') }}
    </span>
</a>
