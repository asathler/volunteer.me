<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    {{-- Tell the browser to be responsive to screen width --}}
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }} | Blank Page</title>

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    {{-- icheck bootstrap --}}
    <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    {{-- Toastr --}}
    <link rel="stylesheet" href="{{ asset('plugins/toastr/toastr.min.css') }}">
    {{-- Theme style --}}
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
    {{-- flag-icon-css --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.3.0/css/flag-icon.min.css">
    {{-- Ionicons --}}
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    {{-- Google Font: Source Sans Pro --}}
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700">
</head>

<body class="hold-transition sidebar-mini {{ config('adminlte.links-color') }}">

<div class="wrapper">
    @include('layouts.base.top')

    @include('layouts.base.sidebar')

    @include('layouts.base.main')

    @include('layouts.base.footer')

    @include('layouts.base.control-sidebar')
</div>

{{-- jQuery --}}
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
{{-- Bootstrap 4 --}}
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
{{-- Toastr --}}
<script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
{{-- AdminLTE App --}}
<script src="{{ asset('js/adminlte.min.js') }}"></script>
{{-- Speed up loaded pages --}}
{{-- <script src="https://unpkg.com/turbolinks"></script> --}}

@include('layouts.base.main.toastr')

</body>
</html>
