@php
    $menu = new \App\Aid\Menu();
    $mainMenu = $menu->makeMainMenu();
@endphp

<nav class="mt-2">
    <ul class="nav
               nav-pills
               nav-sidebar
               flex-column
               {{ config('adminlte.menu-text-size') }}
               {{ config('adminlte.menu-compact') }}
               {{ config('adminlte.menu-indent') }}"
        data-widget="treeview"
        role="menu"
        data-accordion="{{ config('adminlte.menu-accordion') }}"
    >

        @foreach($mainMenu as $item)
            @include('layouts.base.sidebar.menu.item')
        @endforeach

    </ul>
</nav>
