<?php

return [
    'login'                 => 'Entrar',
    'login_google'          => 'Entrar com Google',
    'logout'                => 'Sair',
    'password_confirm'      => 'Confirme sua senha',
    'password_forgot'       => 'Esqueceu a senha',
    'password_request'      => 'Solicite uma nova senha',
    'password_reset'        => 'Redefinir senha',
    'register'              => 'Registrar',
    'verify'                => 'Verificação de email',

    'field'                 => [
        'agree'             => 'Eu aceito os',
        'email'             => 'Email',
        'name_full'         => 'Nome completo',
        'password'          => 'Senha',
        'password_confirm'  => 'Confirmação da senha',
        'remember_me'       => 'Lembrar-me',
        'terms'             => 'termos',
    ],

    'title'                 => [
        'login'             => 'Entre com seu e-mail e senha para iniciar sua sessão.',
        'password_confirm'  => 'Confirme sua senha para continuar sua sessão.',
        'password_forgot'   => 'Esqueceu sua senha? Aqui você pode recuperar uma nova senha facilmente.',
        'register'          => 'Crie um novo usuário para acessar o sistema.',
        'verify'            => 'Verique seu endereço de e-mail.',
    ],

    'text'                  => [
        'before_proceeding' => 'Antes de continuar, favor conferir o link de verificação enviado ao seu e-mail.',
        'fresh_email'       => 'Um novo link de verificação foi enviado ao seu endereço de e-mail.',
        'login'             => 'Eu já tenho um usuário.',
        'logout'            => 'Fechar e sair da sua sessão.',
        'no_email'          => 'Se você não recebeu o email,',
        'password_forgot'   => 'Esqueci minha senha.',
        'register'          => 'Registrar um novo usuário.',
    ],

    // 'Before proceeding'     => 'Before proceeding, please check your email for a verification link.',
    // 'Fresh verification'    => 'A fresh verification link has been sent to your email address.',
    // 'Forgot Your Password'  => 'I forgot my password',
    // 'Forgot Full'           => 'You forgot your password? Here you can easily retrieve a new password.',
    // 'Login message'         => 'Sign in to start your session',
    // 'No email'              => 'If you did not receive the email,',
    // 'Register full'         => 'Register a new membership',
    // 'Request another'       => 'click here to request another',
    // 'terms'                 => 'terms',

];
