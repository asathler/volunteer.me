@extends('layouts.auth')

@section('content')
    <p class="login-box-msg">
        {{ __('messages.Sign in to start your session.') }}
    </p>

    <form action="{{ route('login') }}" method="post">
        @csrf

        <div class="input-group mb-3">
            <input id="email"
                   name="email"
                   type="email"
                   placeholder="{{ __('messages.E-Mail Address') }}"
                   class="form-control @error('email') is-invalid @enderror"
                   autocomplete="email"
                   value="{{ old('email') }}"
                   required
                   autofocus>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                </div>
            </div>

            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="input-group mb-3">
            <input id="password"
                   name="password"
                   type="password"
                   placeholder="{{ __('messages.Password') }}"
                   class="form-control @error('password') is-invalid @enderror"
                   autocomplete="current-password"
                   required>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="row">
            <div class="col-7">
                <div class="icheck-primary">
                    <input id="remember"
                           name="remember"
                           type="checkbox"
                           class="form-check-input"
                           {{ old('remember') ? 'checked' : '' }}>
                    <label for="remember">
                        {{ __('messages.Remember Me') }}
                    </label>
                </div>
            </div>

            <div class="col-5">
                <button type="submit" class="btn btn-primary btn-block">
                    @include('auth.inc.login')
                </button>
            </div>
        </div>
    </form>

    <div class="social-auth-links text-center mb-3">
        @include('auth.inc.socials')
    </div>

    <p class="mb-1">
        <a href="/password/reset">
            @include('auth.inc.forgot-password')
        </a>
    </p>

    <p class="mb-0">
        <a href="/register" class="text-center">
            @include('auth.inc.register')
        </a>
    </p>
@endsection
