<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            @include('layouts.base.main.header.title')

            @include('layouts.base.main.header.breadcrumb')
        </div>
    </div>
</section>
