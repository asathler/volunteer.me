@extends('layouts.auth')

@section('content')
    <p class="login-box-msg">
        {{ __('messages.Please confirm your password before continuing.') }}
    </p>

    <form action="{{ route('password.confirm') }}" method="post">
        @csrf

        <input type="hidden" name="token" value="{{ $token ?? ''}}">

        <div class="input-group mb-3">
            <input id="password"
                   name="password"
                   type="password"
                   placeholder="{{ __('messages.Password') }}"
                   class="form-control @error('password') is-invalid @enderror"
                   autocomplete="current-password"
                   required>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>

            @error('password')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="row">
            <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block">
                    {{ __('messages.Confirm Password') }}
                </button>
            </div>
        </div>
    </form>

    <p class="mt-3 mb-0">
        <a href="/password/reset">
            {{ __('messages.Forgot Your Password?') }}
        </a>
    </p>
@endsection
