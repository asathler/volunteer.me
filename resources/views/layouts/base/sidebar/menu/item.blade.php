@php
$childrens = $menu->childrens($item['text']);
$hasSub = $childrens->count() > 0;

$text = Str::ucfirst(__('core.' . $item['text']));
@endphp

<li class="nav-item {{ $hasSub ? 'has-treeview' : '' }}">
    <a href="{{ $item['link'] }}"
       alt="{{ $text }}"
       title="{{ $text }}"
       class="nav-link {{ $item['active'] ? 'active' : '' }}"
    >
        <i class="nav-icon far {{ $item['icon'] }}"></i>
        <p>
            {{ $text }}
            @if ($hasSub)
                <i class="right fas fa-angle-left"></i>
            @endif
        </p>
    </a>

    @if ($hasSub)
        <ul class="nav nav-treeview">
            @foreach ($childrens as $item)
                @include('layouts.base.sidebar.menu.item')
            @endforeach
        </ul>
    @endif
</li>
