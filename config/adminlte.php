<?php

// Possibles sizes
// 'text-sm', 'text-md', 'text-lg', 'text-xl'
$logoTextSize = '';
$navbarTextSize = '';
$menuTextSize = 'text-lg';
$footerTextSize = 'text-sm';

// Possibles styles
// 'nav-compact', ''
// 'nav-child-indent', ''
$menuCompact = 'nav-compact';
$menuIndent = 'nav-child-indent';

// Possibles colors
// 'primary', 'secondary', 'success', 'info', 'warning', 'danger',
// 'blue', 'green', 'cyan', 'yellow', 'red',
// 'black', 'gray-dark', 'gray',
// 'indigo', 'navy', 'purple', 'fuchsia', 'pink', 'maroon', 'orange', 'lime', 'teal', 'olive',
$logoColor = 'red';
$topBarColor = 'red';
$menuColor = 'red';
$linksColor = 'red';
$profileColor = 'red';

// Possibles backgrounds
// 'light', 'dark',
$logoBackground = 'dark';
$topBarBackground = 'dark';
$menuBackground = 'light';

// Possibles accordion options
// true, false
$menuAccordion = true;

return [
    // ********************************************************************************
    // Colors
    // ********************************************************************************
    'logo-color' => 'navbar-dark ' . $logoBackground . ' navbar-' . $logoColor,
    'navbar-color' => 'navbar-dark ' . $topBarBackground . ' navbar-' . $topBarColor,
    'menu-color' => 'sidebar-' . $menuBackground . '-' . $menuColor,
    'links-color' => 'accent-' . $linksColor,
    'profile-color' => 'bg-' . $profileColor,

    // ********************************************************************************
    // Text size
    // ********************************************************************************
    'logo-text-size' => $logoTextSize,
    'navbar-text-size' => $navbarTextSize,
    'menu-text-size' => $menuTextSize,
    'footer-text-size' => $footerTextSize,

    // ********************************************************************************
    // Menu
    // ********************************************************************************
    'menu-compact' => $menuCompact,
    'menu-indent' => $menuIndent,
    'menu-accordion' => $menuAccordion,
];
