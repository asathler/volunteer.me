{{--
<strong>
    {{ __('messages.Copyright') }}
</strong>
--}}
<a href="{{ config('app.url') }}"><strong>{{ env('APP_NAME_BASE') }}</strong>{{ env('APP_NAME_SUFFIX') }}</a>
&copy; 2010-2020.
{{ __('messages.All rights reserved') }}.
