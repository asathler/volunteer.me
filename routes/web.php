<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login/{provider}', 'Auth\LoginController@checkWithProvider');
Route::get('/login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Auth::routes(['verify' => true]);

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');

Route::get('/institution', function() { return 'Institution'; })->name('institution');
Route::get('/institution/departments', function() { return 'Departments'; })->name('departments');
Route::get('/institution/documents', function() { return 'Documents'; })->name('documents');
