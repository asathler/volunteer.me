<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class Settings
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->setAppLocale();

        return $next($request);
    }

    private function setAppLocale()
    {
        $this->setLocale(app()->getLocale());

        // $this->setLocale($organizationLocale = 'es' ?? null);

        if (isset(auth()->user()->settings)) {
            $this->setLocale(auth()->user()->settings->locale);
        }
    }

    private function setLocale($locale)
    {
        if (!is_null($locale)) {
            app()->setLocale($locale);
        }
    }
}
