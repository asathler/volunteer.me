@extends('layouts.auth')

@section('content')
    <p class="login-box-msg">
        {{ __('messages.Verify Your Email Address') }}
    </p>

    @if (session('resent'))
        <div class="alert alert-success" role="alert">
            {{ __('messages.A fresh verification link has been sent to your email address.') }}
        </div>
    @endif

    <div class="input-group mb-3">
        <div class="col-12">
            {{ __('messages.Before proceeding, please check your email for a verification link.') }}
            {{ __('messages.If you did not receive the email') }}
        </div>
    </div>

    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
        @csrf

        <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block">
                <i class="far fa-paper-plane"></i>
                {{ __('messages.Click here to request another') }}
            </button>
        </div>
    </form>

    <div class="input-group mt-3">
        <div class="col-12">
            <a href="{{ route('logout') }}"
               class="text-center"
               onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
                @include('auth.inc.logout')
            </a>
        </div>
    </div>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
@endsection
