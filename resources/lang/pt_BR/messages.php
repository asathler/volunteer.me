<?php

return [
    "A fresh verification link has been sent to your email address." =>
        "Um novo link de verificação foi enviado para seu endereço de e-mail.",
    "All rights reserved" => "Todos os direitos reservados",
    "Before proceeding, please check your email for a verification link." =>
        "Antes de prosseguir, verifique seu e-mail em busca de um link de verificação.",
    "Click here to request another" => "Clique aqui para solicitar outro",
    "Confirm Password" => "Confirmar senha",
    "Copyright" => "Copyright",
    "Dashboard" => "Painel",
    "Email" => "E-mail",
    "E-Mail Address" => "Endereço de e-mail",
    "Forbidden" => "Proibido",
    "Forgot Your Password?" => "Esqueceu a senha?",
    "Full Name" => "Nome Completo",
    "Go Home" => "Ir para o início",
    "Hello!" => "Olá!",
    "I agree to" => "Eu aceito os",
    "If you did not create an account, no further action is required." =>
        "Se você não criou uma conta, ignore este e-mail.",
    "If you did not receive the email" => "Se você não recebeu o e-mail",
    "If you did not request a password reset, no further action is required." =>
        "Se você não solicitou essa redefinição de senha, ignore este e-mail.",
    "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\ninto your web browser:" =>
        "Se você estiver com problemas para clicar no botão \":actionText\", copie e cole o URL abaixo\nem seu navegador da web:",
    "Invalid signature." => "Assinatura inválida.",
    "login" => "Entrar",
    "Login" => "Entrar",
    "Login with Google" => "Entrar com Google",
    "Logout" => "Sair",
    "Name" => "Nome",
    "Not Found" => "Não encontrado",
    "Oh no" => "Ah não",
    "Page Expired" => "Página expirou",
    "Password" => "Senha",
    "Please click the button below to verify your email address." =>
        "Por favor, clique no botão abaixo para verificar seu endereço de e-mail.",
    "Please confirm your password before continuing." => "Por favor, confirme sua senha antes de continuar.",
    "Regards" => "Atenciosamente",
    "Register" => "Registrar",
    "Register a new membership." => "Crie um novo usuário para acessar o sistema.",
    "Remember Me" => "Lembre-se de mim",
    "Reset Password" => "Redefinir senha",
    "Reset Password Notification" => "Notificação de redefinição de senha",
    "Send Password Reset Link" => "Enviar link de redefinição de senha",
    "Server Error" => "Erro do servidor",
    "Service Unavailable" => "Serviço indisponível",
    "Sign in to start your session." => "Entre com seu e-mail e senha para iniciar sua sessão.",
    "terms" => "termos",
    "This action is unauthorized." => "Esta ação não é autorizada.",
    "This password reset link will expire in :count minutes." =>
        "O Link de redefinição de senha irá expirar em :count minutos.",
    "Toggle navigation" => "Alternar navegação",
    "Too Many Attempts." => "Muitas tentativas.",
    "Too Many Requests" => "Muitas solicitações",
    "Unauthorized" => "Não autorizado",
    "Version" => "Versão",
    "Verify Email Address" => "Verifique o endereço de e-mail",
    "Verify Your Email Address" => "Verifique seu endereço de e-mail",
    "We won't ask for your password again for a few hours." =>
        "Não solicitaremos sua senha novamente por algumas horas.",
    "You are logged in!" => "Você já está logado!",
    "You are receiving this email because we received a password reset request for your account." =>
        "Você recebeu esse e-mail porque foi solicitado uma redefinição de senha na sua conta.",
    "You forgot your password? Here you can easily retrieve a new password." =>
        "Esqueceu sua senha? Aqui você pode recuperar uma nova senha facilmente.",
    "Your email address is not verified." => "Seu endereço de e-mail não está verificado.",
    "Whoops!" => "Ops!",

    /*
    "password_confirm" => "Confirme sua senha",
    "password_forgot" => "Esqueceu a senha",
    "password_request" => "Solicite uma nova senha",
    "password_reset" => "Redefinir senha",
    "register" => "Registrar",
    "verify" => "Verificação de email",

    "field" => [
        "agree" => "Eu aceito os",
        "email" => "Email",
        "name_full" => "Nome completo",
        "password" => "Senha",
        "password_confirm" => "Confirmação da senha",
        "remember_me" => "Lembrar-me",
        "terms" => "termos",
    ],

    "title" => [
        "login" => "Entre com seu e-mail e senha para iniciar sua sessão.",
        "password_confirm" => "Confirme sua senha para continuar sua sessão.",
        "password_forgot" => "Esqueceu sua senha? Aqui você pode recuperar uma nova senha facilmente.",
        "register" => "Crie um novo usuário para acessar o sistema.",
        "verify" => "Verique seu endereço de e-mail.",
    ],

    "text" => [
        "before_proceeding" => "Antes de continuar, favor conferir o link de verificação enviado ao seu e-mail.",
        "fresh_email" => "Um novo link de verificação foi enviado ao seu endereço de e-mail.",
        "login" => "Eu já tenho um usuário.",
        "logout" => "Fechar e sair da sua sessão.",
        "no_email" => "Se você não recebeu o email,",
        "password_forgot" => "Esqueci minha senha.",
        "register" => "Registrar um novo usuário.",
    ],

    // "Before proceeding"     => "Before proceeding, please check your email for a verification link.",
    // "Fresh verification"    => "A fresh verification link has been sent to your email address.",
    // "Forgot Your Password"  => "I forgot my password",
    // "Forgot Full"           => "You forgot your password? Here you can easily retrieve a new password.",
    // "Login message"         => "Sign in to start your session",
    // "No email"              => "If you did not receive the email,",
    // "Request another"       => "click here to request another",
    */
];
