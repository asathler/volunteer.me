<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

        {{-- ORGANIZATION --}}
        <li class="nav-item has-treeview">
            <a href="" class="nav-link">
                <i class="nav-icon far fa-hospital"></i>
                <p>
                    {{ __('Organization') }}
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-store-alt"></i>
                        <p>
                            {{ __('Institution') }}
                        </p>
                    </a>
                </li>
            </ul>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-bezier-curve"></i>
                        <p>
                            {{ __('Departments') }}
                        </p>
                    </a>
                </li>
            </ul>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-file-alt"></i>
                        <p>
                            {{ __('Documents') }}
                        </p>
                    </a>
                </li>
            </ul>
        </li>

    </ul>
</nav>
