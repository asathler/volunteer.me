<li class="nav-item dropdown user-menu">
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
        <img src="{{ auth()->user()->settings->avatar }}"
             alt="{{ auth()->user()->name }}"
             title="{{ auth()->user()->name }}"
             class="user-image img-circle elevation-2">
    </a>

    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <li class="user-header {{ config('adminlte.profile-color') }}">
            <img src="{{ auth()->user()->settings->avatar }}"
                 alt="{{ auth()->user()->name }}"
                 title="{{ auth()->user()->name }}"
                 class="img-circle elevation-2">
            <p>
                {{ auth()->user()->name }}
                -
                Web Developer
                <small>
                    {{ __('messages.Joined') . ' ' . auth()->user()->created_at->diffForHumans() }}
                </small>
            </p>
        </li>

        {{--
        <li class="user-body">
            <div class="row">
                <div class="col-4 text-center">
                    <a href="#">
                        Followers
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="#">
                        Sales
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="#">
                        Friends
                    </a>
                </div>
            </div>
        </li>
        --}}

        <li class="user-footer">
            <a href="#" class="btn btn-default btn-flat">
                {{ __('messages.Profile') }}
            </a>

            <a href="{{ route('logout') }}"
               class="btn btn-default btn-flat float-right"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                {{ __('messages.Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</li>
