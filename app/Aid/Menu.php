<?php

namespace App\Aid;

use Illuminate\Support\Collection;

class Menu extends Collection
{
    const HOME = '/home';
    const VOID = '#!';

    const ADMIN = '/admin';
    const ADMIN_LOGS = '/admin/logs';
    const ADMIN_ROLES = '/admin/roles';
    const ADMIN_USERS = '/admin/users';
    const ADMIN_PERMISSIONS = '/admin/permissions';
    const ASSETS = '/assets';
    const ASSETS_LOANS = '/assets/loan';
    const ASSETS_FURNITURE = '/assets/furniture';
    const ASSETS_MATERIALS = '/assets/material';
    const ASSETS_PROPERTIES = '/assets/property';
    const ASSETS_VEHICLES = '/assets/vehicle';
    const DASHBOARD = '/dashboard';
    const ELECTIONS = '/elections';
    const ELECTIONS_CAMPAIGNS = '/elections/campaigns';
    const ELECTIONS_CANDIDATES = '/elections/candidates';
    const ELECTIONS_VOTE = '/elections/vote';
    const FINANCE = '/finance';
    const FINANCE_BALANCE = '/finances/balance';
    const FINANCE_DONATIONS = '/finances/donations';
    const FINANCE_PAYMETS = '/finances/payments';
    const FINANCE_SALES = '/finances/sales';
    const FINANCE_STATUS = '/finances/status';
    const INSTITUTION = '/institution';
    const INSTITUTION_CHART = '/institution/chart';
    const INSTITUTION_COUNCIL = '/institution/council';
    const INSTITUTION_DEPARTMENTS = '/institution/departments';
    const INSTITUTION_DOCUMENTS = '/institution/documents';
    const INSTITUTION_TIMELINE = '/institution/timeline';
    const TRAININGS = '/trainings';
    const TRAININGS_CLASS = '/trainings/class';
    const TRAININGS_COURSES = '/trainings/courses';
    const TRAININGS_COURSES_CERTIFICATES = '/trainings/courses/certificates';
    const TRAININGS_INSTRUCTORS = '/trainings/instructors';
    const TRAININGS_SPEECHES = '/speeches';
    const VOLUNTEERING = '/volunteering';
    const VOLUNTEERING_VOLUNTEERS = '/volunteering/volunteers';
    const VOLUNTEERING_CALLS = '/volunteering/calls';
    const VOLUNTEERING_HOURS = '/volunteering/hours';

    const ACTIVITIES = '/activities';
    const EVENTS = '/event';
    const MANAGEMENT = '/management';
    const MEDICINES = '/medicines'; // fas fa-pills
    const PARTNERS = '/partners'; // fas fa-hand-holding-usd
    const PROJECTS = '/projects';


    protected $currentUri;
    protected $currentPermission;

    public function item($text, $link)
    {
        $this->items[] = [
            'text' => $text,
            'link' => $link,
            'icon' => '',
            'parent' => '',
            'permission' => '',
            'can' => true, // check if has permission!
            'show' => true,
            'active' => ($link == $this->currentUri)
        ];

        return $this;
    }

    public function icon($icon)
    {
        $this->addMenuItemElement('icon', $icon);

        return $this;
    }

    public function parent($parent)
    {
        $this->addMenuItemElement('parent', $parent);

        return $this;
    }

    public function permission($permission)
    {
        $this->addMenuItemElement('permission', $permission);

        return $this;
    }

    public function show($isVisible)
    {
        $this->addMenuItemElement('show', $isVisible);

        return $this;
    }

    public function __call($method, $parameters)
    {
        $this->addMenuItemElement($method, $parameters[0]);

        return $this;
    }

    public function makeMainMenu()
    {
        $this->currentUri = Request()->getRequestUri();
        $this->currentPermission = '';

        $this->setMainMenu();

        return $this
            ->where('parent', '')
            ->where('show', true)
            ->where('can', true);
    }

    public function childrens($parent)
    {
        return $this
            ->where('parent', $parent)
            ->where('show', true)
            ->where('can', true);
    }

    private function addMenuItemElement($element, $value)
    {
        $key = count($this->items) - 1;
        $currentItem = last($this->items);
        $newElement = [$element => $value];

        $this->items[$key] = array_merge($currentItem, $newElement);
    }

    private function setMainMenu()
    {
        $this->item('home', self::HOME)
            ->show(true)
            ->icon('fa fa-home');

        $this->item('dashboard', self::DASHBOARD)
            ->icon('fa fa-tachometer-alt');

        $this->item('organization', self::VOID)
            ->icon('fa fa-hospital');

        $this->item('institution', self::INSTITUTION)
            ->icon('fa fa-store')
            ->parent('organization');
        $this->item('chart', self::INSTITUTION_CHART)
            ->show(true)
            ->icon('fas fa-sitemap')
            ->parent('organization');
        $this->item('council', self::INSTITUTION_COUNCIL)
            ->show(true)
            ->icon('fas fa-glasses')
            ->parent('organization');
        $this->item('departments', self::INSTITUTION_DEPARTMENTS)
            ->show(true)
            ->icon('fa fa-bezier-curve')
            ->parent('organization');
        $this->item('timeline', self::INSTITUTION_TIMELINE)
            ->show(true)
            ->icon('far fa-hourglass')
            ->parent('organization');
        $this->item('documents', self::INSTITUTION_DOCUMENTS)
            ->show(true)
            ->icon('far fa-file-alt')
            ->parent('organization');

        $this->item('volunteering', self::VOLUNTEERING)
            ->show(true)
            ->icon('far fa-address-card');

        $this->item('volunteers', self::VOLUNTEERING_VOLUNTEERS)
            ->show(true)
            ->icon('far fa-id-badge')
            ->parent('volunteering');
        $this->item('calls', self::VOLUNTEERING_CALLS)
            ->show(true)
            ->icon('fas fa-bullhorn')
            ->parent('volunteering');
        $this->item('hours', self::VOLUNTEERING_HOURS)
            ->show(true)
            ->icon('far fa-clock')
            ->parent('volunteering');

        $this->item('trainings', self::TRAININGS)
            ->show(true)
            ->icon('fas fa-graduation-cap');

        $this->item('courses', self::TRAININGS_COURSES)
            ->show(true)
            ->icon('fas fa-book')
            ->parent('trainings');
        $this->item('classes', self::TRAININGS_CLASS)
            ->show(true)
            ->icon('fas fa-chalkboard')
            ->parent('trainings');
        $this->item('speeches', self::TRAININGS_SPEECHES)
            ->show(true)
            ->icon('fas fa-tv')
            ->parent('trainings');
        $this->item('instructors', self::TRAININGS_INSTRUCTORS)
            ->show(true)
            ->icon('fas fa-chalkboard-teacher')
            ->parent('trainings');
        $this->item('certificates', self::TRAININGS_COURSES_CERTIFICATES)
            ->show(true)
            ->icon('fas fa-award')
            ->parent('trainings');

        $this->item('elections', self::ELECTIONS)
            ->show(true)
            ->icon('fas fa-vote-yea');

        $this->item('campaigns', self::ELECTIONS_CAMPAIGNS)
            ->show(true)
            ->icon('far fa-calendar-alt')
            ->parent('elections');
        $this->item('candidates', self::ELECTIONS_CANDIDATES)
            ->show(true)
            ->icon('fas fa-people-arrows')
            ->parent('elections');
        $this->item('vote', self::ELECTIONS_VOTE)
            ->show(true)
            ->icon('far fa-check-square')
            ->parent('elections');

        $this->item('assets', self::ASSETS)
            ->show(true)
            ->icon('fas fa-warehouse');

        $this->item('properties', self::ASSETS_PROPERTIES)
            ->show(true)
            ->icon('far fa-building')
            ->parent('assets');
        $this->item('vehicles', self::ASSETS_VEHICLES)
            ->show(true)
            ->icon('fas fa-car')
            ->parent('assets');
        $this->item('furniture', self::ASSETS_FURNITURE)
            ->show(true)
            ->icon('fas fa-couch')
            ->parent('assets');
        $this->item('materials', self::ASSETS_MATERIALS)
            ->show(true)
            ->icon('fas fa-dolly')
            ->parent('assets');
        $this->item('loans', self::ASSETS_LOANS)
            ->show(true)
            ->icon('fas fa-truck-loading')
            ->parent('assets');

        $this->item('financial', self::FINANCE)
            ->show(true)
            ->icon('fas fa-money-check-alt');

        $this->item('balance', self::FINANCE_BALANCE)
            ->show(true)
            ->icon('fas fa-cogs')
            ->parent('financial');
        $this->item('donations', self::FINANCE_DONATIONS)
            ->show(true)
            ->icon('fas fa-wallet')
            ->parent('financial');
        $this->item('payments', self::FINANCE_PAYMETS)
            ->show(true)
            ->icon('far fa-money-bill-alt')
            ->parent('financial');
        $this->item('sales', self::FINANCE_SALES)
            ->show(true)
            ->icon('fas fa-cash-register')
            ->parent('financial');
        $this->item('status', self::FINANCE_STATUS)
            ->show(true)
            ->icon('fas fa-search-dollar')
            ->parent('financial');

        $this->item('admin', self::ADMIN)
            ->show(true)
            ->icon('fas fa-cogs');

        $this->item('users', self::ADMIN_USERS)
            ->show(true)
            ->icon('fas fa-user')
            ->parent('admin');
        $this->item('roles', self::ADMIN_ROLES)
            ->show(true)
            ->icon('fas fa-user-tag')
            ->parent('admin');
        $this->item('permissions', self::ADMIN_PERMISSIONS)
            ->show(true)
            ->icon('fa fa-traffic-light')
            ->parent('admin');
        $this->item('logs', self::ADMIN_LOGS)
            ->show(true)
            ->icon('fas fa-terminal')
            ->parent('admin');
    }
}
