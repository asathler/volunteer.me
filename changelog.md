<p align="center">
.![Logo](public/img/logo_volunteer.png)
</p>

# Changelog

All major changes made are reported here.

## v0.0.4 - 2020-07-xx
* Base layout done
* Uses AdminLte v3 Bootstrap 4 front-end Framework

## v0.0.0 - 2020-07-01
* Multi-lang support with english as mais lang
* User auth, and Google oAuth integration
* Start project coding with Laravel PHP Framework
