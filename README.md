<p align="center">
.![Logo](public/img/logo_volunteer.png)
</p>

# Volunteer.me

Volunteer, organization and their actions management.

## License

For now, this is an ideal under development for a SaaS project to maintain most of the major desired functionality to 
to run a volunteer organization.

## Languages

This project try to stay fully in **english**.

In some future version the multi-language function will be add.

Minimal language desirables in v.2.0 are:
* Português (Brasil);
* English; and
* Español.

## Versions

There is no formal versioning at the moment.

## Project and source code

* PHP 7.3
  * Laravel 7.0
* Postgres 12.x

## Installation and config

To be informed...

## Contributing

In a few weeks...
