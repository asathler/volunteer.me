<aside class="main-sidebar {{ config('adminlte.menu-color') }} elevation-4">
    @include('layouts.base.sidebar.logo')

    <div class="sidebar">
        @include('layouts.base.sidebar.user')

        @include('layouts.base.sidebar.menu')
    </div>
</aside>
