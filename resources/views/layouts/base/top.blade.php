<nav class="main-header
            navbar
            navbar-expand
            {{ config('adminlte.navbar-color') }}
            {{ config('adminlte.navbar-text-size') }}">
    @include('layouts.base.top.left-menu')

    @include('layouts.base.top.search')

    <ul class="navbar-nav ml-auto">
        @include('layouts.base.top.dropdown.messages')

        @include('layouts.base.top.dropdown.notifications')

        @include('layouts.base.top.dropdown.languages')

        @include('layouts.base.top.dropdown.user')
    </ul>
</nav>
