<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSettings extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_uuid', 'avatar', 'locale',
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
