@extends('layouts.auth')

@section('content')
    <p class="login-box-msg">
        {{ __('messages.You forgot your password? Here you can easily retrieve a new password.') }}
    </p>

    <form action="{{ route('password.email') }}" method="post">
        @csrf

        <div class="input-group mb-3">
            <input id="email"
                   name="email"
                   type="email"
                   placeholder="{{ __('messages.E-Mail Address') }}"
                   class="form-control @error('email') is-invalid @enderror"
                   autocomplete="email"
                   value="{{ old('email') }}"
                   required
                   autofocus>
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                </div>
            </div>

            @error('email')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="row">
            <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block">
                    <i class="far fa-paper-plane"></i>
                    {{ __('messages.Send Password Reset Link') }}
                </button>
            </div>
        </div>
    </form>

    <p class="mt-3 mb-1">
        <a href="/login" class="text-center">
            @include('auth.inc.login')
        </a>
    </p>

    <p class="mb-0">
        <a href="/register" class="text-center">
            @include('auth.inc.register')
        </a>
    </p>
@endsection
