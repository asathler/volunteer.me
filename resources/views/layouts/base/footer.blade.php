<footer class="main-footer {{ config('adminlte.footer-text-size') }}">
    @include('layouts.base.footer.copyright')

    @include('layouts.base.footer.version')
</footer>
