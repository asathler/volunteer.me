<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        /*
        \Illuminate\Support\Facades\Session::flash('alert-danger', 'danger');
        \Illuminate\Support\Facades\Session::flash('alert-warning', 'warning');
        \Illuminate\Support\Facades\Session::flash('alert-success', 'success');
        \Illuminate\Support\Facades\Session::flash('alert-info', 'info');
        */

        // // // return redirect('home')->with('message', 'success|Record updated.');

        // return redirect::to(route('home'))->with('message', 'message|Record updated.');
        // Redirect::to(route('home'))->with('message', 'success|Record updated.');

        // return view('home')->with('message', 'success|Record updated.');
        $messages = [
            [
                'type' => 'success',
                'title' => 'Message title',
                'message' => 'Record updated.'
            ]
        ];

        // return view('home')->withError('error 1');
        // return view('home')->withError(['error 1', 'error 2']);
        return view('home')->withError('error 1');
        return view('home')->withErrors(['error 1', 'error 2']);

        // return view('home')->withWarning(['warning 1', 'warning 2']);
        // return view('home')->withSuccess('success 1');
        return view('home', compact('messages'));
    }
}
